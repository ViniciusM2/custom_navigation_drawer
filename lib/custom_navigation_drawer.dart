library custom_navigation_drawer;

import 'package:flutter/material.dart';
import 'package:get/get.dart';

/// Drawer for the app
class CustomNavigationDrawer extends StatelessWidget {
  const CustomNavigationDrawer({
    Key? key,
    required this.title,
    this.subtitle = '',
    this.listTiles = const [],
    this.avatar = const CircleAvatar(),
    this.onSubtitleClicked,
  }) : super(key: key);

  /// Title
  final String title;
  final String subtitle;
  final List<ListTile> listTiles;
  final CircleAvatar avatar;
  final VoidCallback? onSubtitleClicked;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0,
      child: ListView(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DrawerHeader(
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      avatar,
                      const SizedBox(height: 10),
                      Text(
                        title,
                        style: Get.theme.textTheme.subtitle1,
                      ),
                      const SizedBox(height: 5),
                      if (onSubtitleClicked != null)
                        MouseRegion(
                          cursor: SystemMouseCursors.click,
                          child: GestureDetector(
                            onTap: onSubtitleClicked,
                            child: Text(
                              subtitle,
                              style: Get.theme.textTheme.subtitle1!.apply(
                                decoration: TextDecoration.underline,
                                color: Get.theme.colorScheme.secondary,
                              ),
                            ),
                          ),
                        )
                      else
                        Text(
                          subtitle,
                          style: Get.theme.textTheme.subtitle1,
                        ),
                    ],
                  ),
                )
              ],
            ),
          ),
          ...listTiles,
        ],
      ),
    );
  }
}
