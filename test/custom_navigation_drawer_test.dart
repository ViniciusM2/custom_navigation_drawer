import 'package:custom_navigation_drawer/custom_navigation_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Tests with guest drawer', () {
    testWidgets('Drawer shows welcome message', (WidgetTester tester) async {
      // ignore: prefer_const_declarations
      final String title = 'Bem-vindo, visitante!';

      final scaffoldKey = GlobalKey<ScaffoldState>();

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            drawer: CustomNavigationDrawer(
              key: UniqueKey(),
              title: title,
            ),
            body: Container(),
          ),
        ),
      );

      scaffoldKey.currentState!.openDrawer();
      await tester.pump();

      final userNameFinder = find.text(title);
      expect(userNameFinder, findsOneWidget);
    });

    testWidgets('Drawer shows subtitle if subtitle is provided',
        (WidgetTester tester) async {
      // ignore: prefer_const_declarations
      final String subtitle = 'Acesse o menu lateral para navegar pelo app.';

      final scaffoldKey = GlobalKey<ScaffoldState>();

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            drawer: CustomNavigationDrawer(
              key: UniqueKey(),
              title: 'Bem-vindo, visitante!',
              subtitle: subtitle,
              onSubtitleClicked: () {},
            ),
            body: Container(),
          ),
        ),
      );

      scaffoldKey.currentState!.openDrawer();
      await tester.pump();

      final userNameFinder = find.text(subtitle);
      expect(userNameFinder, findsOneWidget);
    });

    testWidgets('Drawer shows logout option', (WidgetTester tester) async {
      final scaffoldKey = GlobalKey<ScaffoldState>();

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            drawer: const CustomNavigationDrawer(
              title: 'Sair',
            ),
            body: Container(),
          ),
        ),
      );

      scaffoldKey.currentState!.openDrawer();
      await tester.pump();

      final titleFinder = find.text('Sair');
      expect(titleFinder, findsOneWidget);
    });
    testWidgets('Drawer shows seven textual elements',
        (WidgetTester tester) async {
      final scaffoldKey = GlobalKey<ScaffoldState>();

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            body: Container(),
            drawer: const CustomNavigationDrawer(
              title: '1',
              listTiles: [
                ListTile(
                  title: Text('2'),
                ),
                ListTile(
                  title: Text('3'),
                ),
                ListTile(
                  title: Text('4'),
                ),
                ListTile(
                  title: Text('5'),
                ),
                ListTile(
                  title: Text('6'),
                ),
                ListTile(
                  title: Text('7'),
                ),
              ],
            ),
          ),
        ),
      );

      scaffoldKey.currentState!.openDrawer();
      await tester.pump();

      final finders = [
        find.text('1'),
        find.text('2'),
        find.text('3'),
        find.text('4'),
        find.text('5'),
        find.text('6'),
        find.text('7'),
      ];

      void verify(finder) {
        expect(finder, findsOneWidget);
      }

      finders.forEach(verify);
    });
    testWidgets('Drawer shows specific 7 textual elements',
        (WidgetTester tester) async {
      final scaffoldKey = GlobalKey<ScaffoldState>();

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            body: Container(),
            drawer: const CustomNavigationDrawer(
              title: 'Bem-vindo, visitante!',
              subtitle: 'Autentique-se',
              listTiles: [
                ListTile(
                  title: Text('Início'),
                ),
                ListTile(
                  title: Text('Locais mapeados'),
                ),
                ListTile(
                  title: Text('Solicitar Orçamento'),
                ),
                ListTile(
                  title: Text('Quem Somos'),
                ),
                ListTile(
                  title: Text('Contato'),
                ),
              ],
            ),
          ),
        ),
      );

      scaffoldKey.currentState!.openDrawer();
      await tester.pump();

      final finders = [
        find.text('Bem-vindo, visitante!'),
        find.text('Autentique-se'),
        find.text('Início'),
        find.text('Locais mapeados'),
        find.text('Solicitar Orçamento'),
        find.text('Quem Somos'),
        find.text('Contato'),
      ];

      void expectToFind(finder) {
        expect(finder, findsOneWidget);
      }

      finders.forEach(expectToFind);
    });

    testWidgets('Drawer has an avatar', (WidgetTester tester) async {
      // Creates a key that will be used to control the scaffold.
      final scaffoldKey = GlobalKey<ScaffoldState>();
      // Creates a key that will be used to search for the avatar.
      final avatarKey = UniqueKey();
      // The avatar widget itself
      final avatar = CircleAvatar(
        key: avatarKey,
      );
      // Renders the test app
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            body: Container(),
            drawer: CustomNavigationDrawer(
              avatar: avatar,
              title: 'Bem-vindo, visitante!',
              subtitle: 'Autentique-se',
              listTiles: const [
                ListTile(
                  title: Text('Início'),
                ),
                ListTile(
                  title: Text('Locais mapeados'),
                ),
                ListTile(
                  title: Text('Solicitar Orçamento'),
                ),
                ListTile(
                  title: Text('Quem Somos'),
                ),
                ListTile(
                  title: Text('Contato'),
                ),
              ],
            ),
          ),
        ),
      );
      // It opens the drawer programatically
      scaffoldKey.currentState!.openDrawer();
      // Waits the drawer opening animation
      await tester.pump();
      // It finds the avatar by its key
      final avatarFinder = find.byKey(avatarKey);
      // Expect to find the avatar.
      expect(avatarFinder, findsOneWidget);
    });

    testWidgets('Drawer shows 5 icons', (WidgetTester tester) async {
      // Creates a key that will be used to control the scaffold.
      final scaffoldKey = GlobalKey<ScaffoldState>();

      // Declaration of five unique keys that will be used to search for the icons.
      final firstIconKey = UniqueKey();
      final secondIconKey = UniqueKey();
      final thirdIconKey = UniqueKey();
      final fourthIconKey = UniqueKey();
      final fifthIconKey = UniqueKey();

      // list of finders to be used in the test
      final finders = [
        find.byKey(firstIconKey),
        find.byKey(secondIconKey),
        find.byKey(thirdIconKey),
        find.byKey(fourthIconKey),
        find.byKey(fifthIconKey),
      ];

      // Renders the test app
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            body: Container(),
            drawer: CustomNavigationDrawer(
              title: 'Bem-vindo, visitante!',
              subtitle: 'Autentique-se',
              listTiles: [
                ListTile(
                  title: const Text('Início'),
                  leading: Icon(
                    Icons.home,
                    key: firstIconKey,
                  ),
                ),
                ListTile(
                  title: const Text(
                    'Locais mapeados',
                  ),
                  leading: Icon(
                    Icons.map,
                    key: secondIconKey,
                  ),
                ),
                ListTile(
                  title: const Text('Solicitar Orçamento'),
                  leading: Icon(
                    Icons.money,
                    key: thirdIconKey,
                  ),
                ),
                ListTile(
                  title: const Text('Quem Somos'),
                  leading: Icon(
                    Icons.person,
                    key: fourthIconKey,
                  ),
                ),
                ListTile(
                  title: const Text('Contato'),
                  leading: Icon(
                    Icons.phone,
                    key: fifthIconKey,
                  ),
                ),
              ],
            ),
          ),
        ),
      );

      // It opens the drawer programatically
      scaffoldKey.currentState!.openDrawer();
      // Waits the drawer opening animation
      await tester.pump();

      void verify(finder) {
        expect(finder, findsOneWidget);
      }

      // for each finder in the list it tests if it finds the icon
      finders.forEach(verify);
    });
  });
}
